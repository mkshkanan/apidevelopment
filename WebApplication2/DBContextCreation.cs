﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Configuration;
using WebApplication2.Models;

namespace WebApplication2
{
    public class DBContextCreation : DbContext
    {
        public DbSet<Category> Categories { get; set; }

        public DbSet<SubCategory> SubCategories { get; set; }

        public DbSet<Item> Items { get; set; }
        
    }
}