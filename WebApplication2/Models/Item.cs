﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Item
    {
        /// <summary>
        /// Gets the unique identifier.          
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        [StringLength(12, MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary> 
        public string Description { get; set; }

        public string SubCategoryId { get; set; }

        public SubCategory SubCategory { get; set; }
    }
}