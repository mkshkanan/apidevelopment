﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Dao
{
    public class CategoryDao
    {
        /// <summary>
        /// Delete the Category By Name.
        /// </summary>
        /// <param name="name">Can be null or Empty.</param>
        /// <returns></returns>
        public void DeleteCategory(string name)
        {
            try
            {

                using (DBContextCreation context = new DBContextCreation())
                {
                    var category = context.Categories.Where(i => i.Name.Equals(name));
                    context.Categories.RemoveRange(category.ToList());
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }
    }
}