﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.Models;
using System.Linq;
using System.Linq.Expressions;

namespace WebApplication2.Dao
{
    public class ItemDao
    {
        /// <summary>
        /// Gets the Items By Name.
        /// </summary>
        /// <param name="name">Can be null or Empty.</param>
        /// <returns></returns>
        public List<Item> GetItemByName(string name)
        {
            try
            {

                using (DBContextCreation context = new DBContextCreation())
                {
                    if (!string.IsNullOrEmpty(name))
                        return context.Items.Where(i => i.Name.Equals(name)).ToList();
                    else
                        return context.Items.ToList();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        
    }
}