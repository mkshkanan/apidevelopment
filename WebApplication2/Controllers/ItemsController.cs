﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication2.Dao;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class ItemsController : ApiController
    {
        /// <summary>
        /// Gets the Item by Name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/GetItemsByName/{name?}")]
        public List<Item> GetItemsByName(string name = null)
        {
            ItemDao dao = new ItemDao();
            return dao.GetItemByName(name);                        
        }

        /// <summary>
        /// Delete the Category by Name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/DeleteCategory/{name}")]
        public IHttpActionResult DeleteCategory(string name)
        {
            return Ok();
        }
    }
}
